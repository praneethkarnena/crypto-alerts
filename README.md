# Crypto Alerts

Get alerts for Cryptocurrency prices, for every minute or hour. Bitcoin, Etherum and Monero are supported at this time. 

Get alerts to Email and Mobile. Mobile alerts are only available for people residing in India. 

Technologies: **`Python`**, **`Django`** & **`MongoDB`**
