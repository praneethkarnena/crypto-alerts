from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('about/', views.about),
    path('login/', views.login),
    path('register/', views.register),
    path('contact/', views.contact),
    path('services/', views.services),
    path('profile/', views.profile),
    path('dbtest/', views.dbtest),
    path('logout/', views.logout),
    path('profile/edit/', views.editProfile),
    path('dbdelete/', views.dbDelete),
]