from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from pymongo import MongoClient
import uuid
import requests
import json
from mailjet_rest import Client
import time, schedule

# Create your views here.
def dbCon():
    client = MongoClient("mongodb://127.0.0.1:27017")
    my_database = client.cryptodb
    my_collection = my_database["alerts"]
    return client, my_collection

def getParams(request):
    response = request.POST.items()
    params = {}
    for key, value in response:
        params[key] = value
    return params

def index(request):
    
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def login(request):
    if request.method == "GET":
        if request.session.get("eid") and request.session.get("sid"):
            return HttpResponseRedirect("/profile/")
        if request.GET.get("fpEmail"):
            forgottenEmail = request.GET.get("fpEmail")
            fpClient, fpCollection = dbCon()
            emailExists = fpCollection.find_one({"email": forgottenEmail})
            fpClient.close()
            if emailExists != None:
                API_KEY = "448ac7aedb89bef7785ac7c2701f218c"
                API_SECRET = "9d80c98b25f3c0f767627f8814727ca2"
                mailjet = Client(auth=(API_KEY, API_SECRET))
                message = "Here is your Password: " + emailExists["password"]
                emailData = {
                    "FromName": "Crypto Alerts",
                    "FromEmail": "praneeth@dustmoon.com",
                    "Subject": "Reset Your Password - Crypto Alerts",
                    "Text-Part": message,
                    "Recipients": [{"Email": emailExists["email"]}]
                }
                mailjet.send.create(emailData)
        return render(request, 'login.html')
    if request.method == "POST":
        loginparams = getParams(request)
        userEmail = loginparams["email"]
        userPassword = loginparams["password"]
        client, my_collection= dbCon()
        credCheck = my_collection.find_one({
            "email":userEmail,
            "password":userPassword
        })
        if credCheck != None:
            session_id = str(uuid.uuid4())
            request.session["sid"]=session_id
            request.session["eid"]=userEmail
            my_collection.update_one({
                "email":userEmail
            },{
                "$push":{"session_id":session_id}
            })
            client.close()
            return HttpResponseRedirect("/profile/")
        else:
            return HttpResponseRedirect('/register/')

        
    

def register(request):
    if request.method == "GET":
        if request.session.get("eid") and request.session.get("sid"):
            return HttpResponseRedirect("/profile/")
        return render(request, 'register.html')
    if request.method == "POST":
        regparams = getParams(request)
        userName = regparams["name"]
        userEmail = regparams["email"]
        userPassword = regparams["password"]
        userPhone = regparams["phone"]
        insertCoins = []
        coinsList = ["btc", "eth", "xmr", "ltc"]
        i = 0
        while i < len(coinsList):
            if coinsList[i] in regparams:
                insertCoins.append(regparams[coinsList[i]])
            i = i+1
        client,my_collection = dbCon()
        emailCheck = my_collection.find_one({
            "email":userEmail
        })
        custPlan = regparams["price"]
    if emailCheck == None:
        my_collection.insert_one({
                "name" : userName,
                "email": userEmail,
                "password": userPassword,
                "session_id":[],
                "coins":insertCoins,
                "phone": userPhone,
                "plan": custPlan,
                "paid": "False"
                })
        client.close()
        return HttpResponseRedirect('/login/')
    else:
        return render(request, "register.html", {"userExists": "True"})

def services(request):
    return render(request,'services.html')

def logout(request):
    if request.session.get("eid") and request.session.get("sid"):
        emailID = request.session.get("eid")
        sessionID = request.session.get("sid")
        client, collection = dbCon()
        collection.update_one(
            {"email": emailID},
            {"$pull": {"session_id": sessionID}}
        )
        client.close()
        del request.session["eid"]
        del request.session["sid"]
        return HttpResponseRedirect("/login/")
    return HttpResponseRedirect("/login/")


def contact(request):
    if request.GET.get("cfname") and request.GET.get("cfemail") and request.GET.get("cfmsg"):
        name = request.GET.get("cfname")
        email = request.GET.get("cfemail")
        msg = "From: " + email + " Message: " + request.GET.get("cfmsg")
        API_KEY = "448ac7aedb89bef7785ac7c2701f218c"
        API_SECRET = "9d80c98b25f3c0f767627f8814727ca2"
        mailjet = Client(auth=(API_KEY, API_SECRET))
        emailData = {
        "FromName": name,
        "FromEmail": "praneeth@dustmoon.com",
        "Subject": "Message From a Visitor",
        "Text-Part": msg,
        "Recipients": [{"Email": "navyagarbhapu@gmail.com"}]
        }
        response = mailjet.send.create(emailData)
        print(response)
        return render(request, "contact.html", {"message": "We'll contact you back!"})
    return render(request, "contact.html")

def profile(request):
    if request.session.get("eid") and request.session.get("sid"):
        userEmail=request.session.get("eid")
        userSessionID=request.session.get("sid")
        client, my_collection = dbCon()
        sessionCheck =my_collection.find_one({
            "email":userEmail,
            "session_id":userSessionID 
        })
        if sessionCheck != None:
            if sessionCheck["paid"] == "False" and sessionCheck["plan"] != "FREE":
                if not request.GET.get("payment_id"):
                    userPlan = sessionCheck["plan"]
                    pricing = {
                        "SILVER": "500",
                        "GOLD": "3000"
                    }
                    headers = {
                        "X-Api-Key": "test_055266bc01ad59409af30ef8663",
                        "X-Auth-Token": "test_5596d8c7535346adf4bbe4a7114"
                    }
                    payload = {
                        "buyer_name": sessionCheck["name"],
                        "phone": sessionCheck["phone"],
                        "amount": pricing[userPlan],
                        "purpose":'Crypto Alerts',
                        "send_email": "True",
                        'send_sms': 'True',
                        "email":sessionCheck["email"],
                        'allow_repeated_payments': 'False',
                        "redirect_url":"http://127.0.0.1:8000/profile/"
                    }
                    paymentResponse = requests.post("https://test.instamojo.com/api/1.1/payment-requests/", data=payload, headers=headers).json()
                    paymentURL = paymentResponse["payment_request"]["longurl"]
                    return render(request, "profile.html", {"paid": "False", "payment_url": paymentURL})
                elif request.GET.get("payment_id"):
                    pid = request.GET.get("payment_id")
                    statusheaders = {
                        "X-Api-Key": "test_055266bc01ad59409af30ef8663",
                        "X-Auth-Token": "test_5596d8c7535346adf4bbe4a7114"
                    }
                    url = "https://test.instamojo.com/api/1.1/payments/" + pid + "/"
                    statusResponse = requests.get(url, headers=statusheaders).json()
                    paymentDone = statusResponse["payment"]["status"]
                    if paymentDone == "Credit":
                        my_collection.update_one(
                            {"email": sessionCheck["email"]},
                            {"$set": {"paid": "True"}}
                        )
                        return HttpResponseRedirect("/profile/")
                    return HttpResponseRedirect("/profile/")
            elif sessionCheck["paid"] == "True" or sessionCheck["plan"] == "FREE":
                selectedCoins = sessionCheck["coins"]
                baseAPIURL = "https://min-api.cryptocompare.com/data/pricemulti?fsyms="
                coinsCount = len(selectedCoins)
                while coinsCount > 1:
                    for i in range(0, coinsCount):
                        baseAPIURL = baseAPIURL + selectedCoins[i]
                        if i != coinsCount-1:
                            baseAPIURL = baseAPIURL + ","
                    break 
                if coinsCount == 1:
                    baseAPIURL = baseAPIURL + selectedCoins[0]
                baseAPIURL = baseAPIURL + "&tsyms=INR"
                response = requests.get(baseAPIURL)
                cryptoData = response.json()
                coinName = []
                coinPrice = []
                for coin in cryptoData:
                    coinName.append(coin)
                    for price in cryptoData[coin]:
                        coinPrice.append(cryptoData[coin][price])
                fullData = {}
                for i in range(0, len(coinName)):
                    fullData[coinName[i]] = coinPrice[i]
                emailPriceData = str(fullData)
                sendMail(sessionCheck["email"], emailPriceData)
                sendSMS(sessionCheck["phone"], emailPriceData)
                alertsDuration = {
                    "FREE": 43200,
                    "SILVER": 10800,
                    "GOLD": 300
                }
                alertIn = alertsDuration[sessionCheck["plan"]]
                return render(request, "profile.html", {"paid":"True", "username":sessionCheck["name"], "data": fullData, "alertIn": alertIn})
        client.close()
    else:
        return HttpResponseRedirect("/login/")

def sendMail(senderEmail, emailPriceData):
    API_KEY = "448ac7aedb89bef7785ac7c2701f218c"
    API_SECRET = "9d80c98b25f3c0f767627f8814727ca2"
    mailjet = Client(auth=(API_KEY, API_SECRET))
    emailData = {
        "FromName": "Crypto Alerts",
        "FromEmail": "praneeth@dustmoon.com",
        "Subject": "Crytpo Alerts - Coins Prices",
        "Text-Part": emailPriceData,
        "Recipients": [{"Email": senderEmail}]
    }
    mailjet.send.create(emailData)

def sendSMS(phone, message):
    url = "https://smsapi.engineeringtgr.com/send/?Mobile=9949716014&Password=E3292E&Message=Crypto Alert: " + message + "&To=" + phone + "&Key=navyaGMxKthfNalQqVsJ"
    response = requests.get(url)
    print(url)
    print(response.json())

def editProfile(request):
    if request.method == "GET":
        if request.session.get("eid") and request.session.get("sid"):
            email = request.session.get("eid")
            client, collection = dbCon()
            queryData = collection.find_one({
                "email": email
            })
            client.close()
            if queryData != None:
                name = queryData["name"]
                email = request.session.get("eid")
            return render(request, "edit.html", {"name": name, "email": email})
        else:
            return HttpResponseRedirect("/login/")
    if request.method == "POST":
        oldEmail = request.session.get("eid")
        print("Old Email: ")
        print(oldEmail)
        profileDetails = getParams(request)
        newName = profileDetails["pfname"]
        newEmail = profileDetails["pfemail"]
        client, collection = dbCon()
        collection.update_one(
            {"email": oldEmail},
            {"$set": {"name": newName, "email": newEmail}}
        )
        client.close()
        request.session["eid"]=newEmail
        return HttpResponseRedirect("/profile/edit/")
    else:
        return HttpResponseRedirect("/login/")

def dbtest(request):
    client, my_collection = dbCon()
    fullData = my_collection.find()
    client.close()
    return HttpResponse(fullData)

def dbDelete(request):
    client, collection = dbCon()
    collection.delete_many({})
    client.close()
    return HttpResponse("!")
